using System;
using UnityEngine;

[Serializable]
public struct SpaceRect {
    [SerializeField]
    SpaceCoord _min;
    [SerializeField]
    SpaceCoord _max;

    public SpaceCoord min { get { return _min; } }
    public SpaceCoord max { get { return _max; } }

    public SpaceRect(SpaceCoord min, SpaceCoord max) {
        _min = min;
        _max = max;
    }

    public bool Contains(SpaceCoord coord) {
        return min.x <= coord.x && max.x >= coord.x && min.y <= coord.y && max.y >= coord.y;
    }
}
