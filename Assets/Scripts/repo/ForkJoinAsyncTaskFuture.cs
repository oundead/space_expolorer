using System.Threading;

public class ForkJoinAsyncTaskFuture<TR, R> : AbstractSimpleFuture<R> {
    readonly TR[] forkTaskResults;
    readonly System.Func<int, TR> forkTask;
    readonly System.Func<TR[], R> joinTask;
    int counter;

    public ForkJoinAsyncTaskFuture(int forkCount, System.Func<int, TR> forkTask, System.Func<TR[], R> joinTask) {
        this.forkTask = forkTask;
        this.joinTask = joinTask;

        forkTaskResults = new TR[forkCount];

        // FORK
        counter = forkCount;
        Thread.MemoryBarrier();
        for (var i = 0; i < forkCount; i++) {
            ThreadPool.QueueUserWorkItem(ForkTaskCallback, i);
        }
    }

    void ForkTaskCallback(object state) {
        try {
            Thread.MemoryBarrier();
            int number = (int)state;
            forkTaskResults[number] = forkTask(number);
            Thread.MemoryBarrier();
            // JOIN
            if (Interlocked.Decrement(ref counter) == 0) {
                Thread.MemoryBarrier();
                R result = joinTask(forkTaskResults);
                Complete(result);
            }
        } catch (System.Exception e) {
            UnityEngine.Debug.LogError(e);
        }
    }
}
