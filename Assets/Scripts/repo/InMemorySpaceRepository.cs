﻿using System.Collections.Generic;
using System.Threading;
using System;

public class InMemorySpaceRepository : AbstractSpaceRepository {
    const int MAX_PLANET_RATING = 10000;
    const int PAGE_SIZE = 256;
    const float FILL_RATE = 0.3f;

    public readonly int spacecraftRating = new Random().Next() % MAX_PLANET_RATING + 1;
    readonly Comparison<Planet> comparer;
    readonly Dictionary<SpaceCoord, Planet[]> pages = new Dictionary<SpaceCoord, Planet[]>();
    readonly object monitor = new object();

    public InMemorySpaceRepository() {
        comparer = (p1, p2) => p1.RelativeRating(spacecraftRating) - p2.RelativeRating(spacecraftRating);
        Thread.MemoryBarrier();
    }

    Planet[] GetPage(SpaceCoord pageKey) {
        Planet[] page = null;
        lock (monitor) {
            if (!pages.TryGetValue(pageKey, out page)) {
                page = null;
            }
        }
        if (page != null) {
            return page;
        }
        List<Planet> planetList = new List<Planet>((int)(PAGE_SIZE * PAGE_SIZE * FILL_RATE));
        Random random = new Random();
        for (int x = pageKey.x; x < pageKey.x + PAGE_SIZE; x++) {
            for (int y = pageKey.y; y < pageKey.y + PAGE_SIZE; y++) {
                if (random.NextDouble() < FILL_RATE) {
                    Planet planet = new Planet(
                       new SpaceCoord(
                           x,
                           y
                       ),
                        (random.Next() % MAX_PLANET_RATING + 1)
                   );
                    planetList.Add(planet);
                }
            }
        }
        planetList.Sort(comparer);
        page = planetList.ToArray();
        lock (monitor) {
            if (pages.ContainsKey(pageKey)) {
                page = pages[pageKey];
            } else {
                pages.Add(pageKey, page);
            }
        }
        return page;
    }

    static SpaceCoord ToPageKey(SpaceCoord coord) {
        int x = ((coord.x / PAGE_SIZE) * PAGE_SIZE);
        if (x > coord.x) {
            x -= PAGE_SIZE;
        }
        int y = ((coord.y / PAGE_SIZE) * PAGE_SIZE);
        if (y > coord.y) {
            y -= PAGE_SIZE;
        }
        return new SpaceCoord(x, y);
    }

    public override IFuture<Planet[]> LoadSpace(SpaceRect rect, int limit) {
        int taskCount = Environment.ProcessorCount;

        var minPageKey = ToPageKey(rect.min);
        var maxPageKey = ToPageKey(rect.max);
        var xPageCount = (maxPageKey.x - minPageKey.x) / PAGE_SIZE + 1;
        var yPageCount = (maxPageKey.y - minPageKey.y) / PAGE_SIZE + 1;
        var allPagesCount = xPageCount * yPageCount;
        var pagesCountPerThread = allPagesCount / taskCount + (allPagesCount % taskCount > 0 ? 1 : 0);

        return new ForkJoinAsyncTaskFuture<List<Planet>, Planet[]>(
            taskCount,
            num => {
                int startPageNum = num * pagesCountPerThread;
                int endPageNum = Math.Min(startPageNum + pagesCountPerThread, allPagesCount);
                List<Planet> taskResult = new List<Planet>(limit);
                for (int n = startPageNum; n < endPageNum; n++) {
                    int y = n / xPageCount;
                    int x = n % xPageCount;
                    var pageKey = minPageKey + new SpaceCoord(x * PAGE_SIZE, y * PAGE_SIZE);

                    Planet[] page = GetPage(pageKey);
                    foreach (var p in page) {
                        if (rect.Contains(p.coord)) {
                            var addeded = AddByRR(taskResult, p, limit);
                            if (!addeded) {
                                break;
                            }
                        }
                    }
                }
                return taskResult;
            },
            forkResults => {
                List<Planet> resultList = new List<Planet>(limit);
                foreach (var r in forkResults) {
                    foreach (var p in r) {
                        var addeded = AddByRR(resultList, p, limit);
                        if (!addeded) {
                            break;
                        }
                    }
                }
                return resultList.ToArray();
            }
        );
    }


    bool AddByRR(List<Planet> lst, Planet p, int limit) {
        var prr = p.RelativeRating(spacecraftRating);
        if (lst.Count < limit ||
            lst[lst.Count - 1].RelativeRating(spacecraftRating) > prr) {
            if (lst.Count > limit) {
                lst.RemoveAt(lst.Count - 1);
            }
            int k;
            for (k = 0; k < lst.Count; k++) {
                var lp = lst[k];
                var lrr = lp.RelativeRating(spacecraftRating);
                if (lrr > prr) {
                    break;
                }
            }
            lst.Insert(k, p);
            return true;
        }
        return false;
    }

}
