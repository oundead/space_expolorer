public abstract class AbstractSimpleFuture<R> : IFuture<R> {

    bool _completed;
    R result;
    readonly object monitor;

    protected AbstractSimpleFuture() {
        monitor = new object();
        lock (monitor) {
            _completed = false;
            result = default(R);
        }
    }

    public R value {
        get {
            lock (monitor) {
                return result;
            }
        }
    }

    public bool completed {
        get {
            lock (monitor) {
                return _completed;
            }
        }
    }

    protected void Complete(R result) {
        lock (monitor) {
            this.result = result;
            _completed = true;
        }
    }
}
