﻿using System.Linq;
using UnityEngine;

public class StubSpaceRepositoryImpl : AbstractSpaceRepository {

    public override IFuture<Planet[]> LoadSpace(SpaceRect rect, int limit) {
        return new StubFuture<Planet[]>(
            Enumerable.Range(0, limit > 20 ? limit / 3 : limit)
            .Select(i => new Planet(
                new SpaceCoord(
                    Random.Range(rect.min.x, rect.max.x + 1),
                    Random.Range(rect.min.y, rect.max.y + 1)
                ),
                Random.Range(1, 10001)
            )).ToArray(),
            1
        );
    }

    public class StubFuture<T> : IFuture<T> {

        readonly float completedTime;
        readonly T _value;

        public StubFuture(T value, float delay) {
            this.completedTime = Time.time + delay;
            this._value = value;
        }

        public T value {
            get {
                return _value;
            }
        }

        public bool completed {
            get {
                return completedTime < Time.time;
            }
        }

    }
}
