using System;
using UnityEngine;

[Serializable]
public struct Planet {
    [SerializeField]
    SpaceCoord _coord;
    [SerializeField]
    int _rating;

    public SpaceCoord coord { get { return _coord; } }
    public int rating { get { return _rating; } }

    public Planet(SpaceCoord coord, int rating) {
        this._coord = coord;
        this._rating = rating;
    }

    public int RelativeRating(int rating) {
        return Math.Abs(this.rating - rating);
    }
}
