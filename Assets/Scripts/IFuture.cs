public interface IFuture<T> {
    T value { get; }
    bool completed { get; }
}
