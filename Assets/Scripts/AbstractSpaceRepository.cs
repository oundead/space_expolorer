﻿using UnityEngine;

public abstract class AbstractSpaceRepository : MonoBehaviour {

    public abstract IFuture<Planet[]> LoadSpace(SpaceRect rect, int limit);

}
