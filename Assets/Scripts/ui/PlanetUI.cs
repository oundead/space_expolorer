﻿using UnityEngine;
using UnityEngine.UI;

public class PlanetUI : MonoBehaviour {
#pragma warning disable 649
    [SerializeField]
    Text rating;
#pragma warning restore 649

#pragma warning disable 414
    [SerializeField]
    Planet planet;
    [SerializeField]
    SpaceRect spaceRect;
#pragma warning restore 414

    RectTransform rect;

    void Awake() {
        rect = GetComponent<RectTransform>();
    }

    public void Setup(Planet planet, SpaceRect spaceRect) {
        this.planet = planet;
        this.spaceRect = spaceRect;

        rating.text = planet.rating.ToString();
        RectTransform parentRect = (RectTransform)rect.parent;

        rect.anchoredPosition = new Vector2(
            Mathf.Lerp(
                rect.sizeDelta.x / 2,
                parentRect.sizeDelta.x - rect.sizeDelta.x / 2,
                (float)(planet.coord.x - spaceRect.min.x) / (spaceRect.max.x - spaceRect.min.x)
            ),
            Mathf.Lerp(
                rect.sizeDelta.y / 2,
                parentRect.sizeDelta.y - rect.sizeDelta.y / 2,
                (float)(planet.coord.y - spaceRect.min.y) / (spaceRect.max.y - spaceRect.min.y)
            )
        );
    }

    public void Click() {
        if (transform.parent.childCount - 1 > transform.GetSiblingIndex()) {
            transform.SetAsLastSibling();
        } else {
            transform.SetAsFirstSibling();
        }
    }
}
