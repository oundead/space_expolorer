﻿using UnityEngine;
using UnityEngine.UI;

public class IntField : MonoBehaviour {
#pragma warning disable 649
    [SerializeField]
    InputField input;
    [SerializeField]
    int min;
    [SerializeField]
    int max;
#pragma warning restore 649

    [SerializeField]
    int _value;

    public int value {
        get {
            return _value;
        }
        private set {
            value = Mathf.Clamp(value, min, max);
            if (_value == value) {
                return;
            }
            _value = value;
            if (ValueChanged != null) {
                ValueChanged();
            }
        }
    }

    public event System.Action ValueChanged;

    void Awake() {
        value = value;
        input.text = value.ToString();
    }

    public void EndEdit() {
        int newValue = value;
        if (!int.TryParse(input.text, out newValue)) {
            input.text = value.ToString();
            return;
        }
        value = Mathf.Clamp(newValue, min, max);
        if (value != newValue) {
            input.text = value.ToString();
        }
    }

    public void Inc() {
        value++;
        input.text = value.ToString();
    }

    public void Dec() {
        value--;
        input.text = value.ToString();
    }

}
