﻿using UnityEngine;

public class SpaceModel : MonoBehaviour {
    [SerializeField]
    SpaceRect rect;
    [SerializeField]
    Planet[] planets;

    public SpaceRect Rect {
        get {
            return rect;
        }
        set {
            rect = value;
            OnChange();
        }
    }

    public Planet[] Planets {
        get {
            return planets;
        }
        set {
            planets = value;
            OnChange();
        }
    }

    public event System.Action Changed;

    void OnChange() {
        if (Changed != null) {
            Changed();
        }
    }
}
