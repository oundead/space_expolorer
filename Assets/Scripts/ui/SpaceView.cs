﻿using System.Collections.Generic;
using UnityEngine;

public class SpaceView : MonoBehaviour {
#pragma warning disable 649
    [SerializeField]
    PlanetUI prefab;
    [SerializeField]
    Transform poolTransform;
#pragma warning restore 649

    SpaceModel model;
    List<PlanetUI> active;
    List<PlanetUI> pool;

    void Awake() {
        model = GetComponentInParent<SpaceModel>();
        model.Changed += ModelChanged;
        active = new List<PlanetUI>();
        pool = new List<PlanetUI>();
    }

    void OnDestroy() {
        model.Changed -= ModelChanged;
    }

    void ModelChanged() {
        foreach (var a in active) {
            ReleasePlanetUi(a);
        }
        active.Clear();
        foreach (var p in model.Planets) {
            if (!model.Rect.Contains(p.coord)) {
                continue;
            }
            var ui = GetOrCreatePlanetUi();
            ui.Setup(p, model.Rect);
            active.Add(ui);
        }
    }

    PlanetUI GetOrCreatePlanetUi() {
        PlanetUI ui;
        if (pool.Count > 0) {
            var n = pool.Count - 1;
            ui = pool[n];
            pool.RemoveAt(n);
        } else {
            ui = Instantiate(prefab.gameObject).GetComponent<PlanetUI>();
        }
        ui.transform.SetParent(transform, false);
        return ui;
    }

    void ReleasePlanetUi(PlanetUI ui) {
        ui.transform.SetParent(poolTransform, false);
        pool.Add(ui);
    }

}
