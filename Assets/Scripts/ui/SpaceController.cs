﻿using UnityEngine;

public class SpaceController : MonoBehaviour {
#pragma warning disable 649
    [SerializeField]
    IntField zoom;
    [SerializeField]
    IntField zoomForSwitchMode;
    [SerializeField]
    IntField countLimit;
    [SerializeField]
    IntField coordX;
    [SerializeField]
    IntField coordY;
    [SerializeField]
    Behaviour loadingIndicator;
#pragma warning restore 649

    SpaceModel model;
    AbstractSpaceRepository repository;

    bool needLoad;
    bool loading;
    IFuture<Planet[]> future;

    void Start() {
        model = GetComponentInParent<SpaceModel>();
        repository = GetComponentInParent<AbstractSpaceRepository>();
        zoom.ValueChanged += ValueChanged;
        zoomForSwitchMode.ValueChanged += ValueChanged;
        countLimit.ValueChanged += ValueChanged;
        coordX.ValueChanged += ValueChanged;
        coordY.ValueChanged += ValueChanged;
        ValueChanged();
    }

    void ValueChanged() {
        SpaceCoord min = new SpaceCoord(coordX.value, coordY.value);
        model.Rect = new SpaceRect(min, min + new SpaceCoord(zoom.value, zoom.value) - new SpaceCoord(1, 1));
        needLoad = true;
    }

    void Update() {
        ProcessInput();
        ProcessLoading();
    }

    void ProcessInput() {
        if (Input.GetKey(KeyCode.W)) {
            coordY.Inc();
        }
        if (Input.GetKey(KeyCode.S)) {
            coordY.Dec();
        }
        if (Input.GetKey(KeyCode.D)) {
            coordX.Inc();
        }
        if (Input.GetKey(KeyCode.A)) {
            coordX.Dec();
        }
    }

    void ProcessLoading() {
        if (loading && future != null && future.completed) {
            loading = false;
            model.Planets = future.value;
        }
        if (needLoad && (future == null || future.completed)) {
            loading = true;
            int limit = zoom.value >= zoomForSwitchMode.value ? countLimit.value : (zoom.value * zoom.value);
            future = repository.LoadSpace(model.Rect, limit);
            needLoad = false;
        }
        loadingIndicator.enabled = loading;
    }
}
