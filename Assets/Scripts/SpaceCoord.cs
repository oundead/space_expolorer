﻿using System;
using UnityEngine;

[Serializable]
public struct SpaceCoord : IEquatable<SpaceCoord> {
    [SerializeField]
    int _x;
    [SerializeField]
    int _y;

    public int x { get { return _x; } }
    public int y { get { return _y; } }

    public SpaceCoord(int x, int y) {
        _x = x;
        _y = y;
    }

    public override int GetHashCode() {
        return x << 16 + y;
    }

    public override bool Equals(object obj) {
        return obj != null && obj is SpaceCoord && ((SpaceCoord)obj) == this;
    }

    bool IEquatable<SpaceCoord>.Equals(SpaceCoord other) {
        return this == other;
    }

    public static SpaceCoord operator +(SpaceCoord c1, SpaceCoord c2) {
        return new SpaceCoord((c1.x + c2.x), (c1.y + c2.y));
    }

    public static SpaceCoord operator -(SpaceCoord c1, SpaceCoord c2) {
        return new SpaceCoord((c1.x - c2.x), (c1.y - c2.y));
    }

    public static SpaceCoord operator /(SpaceCoord c1, int d) {
        return new SpaceCoord((c1.x / d), (c1.y / d));
    }

    public static bool operator ==(SpaceCoord c1, SpaceCoord c2) {
        return c1.x == c2.x && c1.y == c2.y;
    }

    public static bool operator !=(SpaceCoord c1, SpaceCoord c2) {
        return c1.x != c2.x || c1.y != c2.y;
    }
}
